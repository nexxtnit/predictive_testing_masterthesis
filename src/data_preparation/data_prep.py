#!/usr/bin/env python3
"""
Preparation of database to make it usable for machine learning
"""

from src.global_tools.logger import global_logger
from pathlib import Path
import sqlite3
import re

import numpy as np
import pandas as pd
from pandas import DataFrame
from natsort import natsort_keygen

from tqdm import tqdm

from sklearn.preprocessing import MultiLabelBinarizer

from thefuzz import process, fuzz
from collections import Counter

from typing import TypedDict
from string import digits
import fnmatch
import toml

remove_digits = str.maketrans("", "", digits)

testresult_row = TypedDict(
    "testresult_row", CommitID=int, TestName=str, Passed=int, Failed=int
)


class DataPreparation:
    def __init__(self, database_name: str, project_config_file: str):
        # init logger
        self.logger = global_logger
        self.logger = self.logger.bind(machine="host")
        self.logger = self.logger.bind(DP=True)

        self.repo_config = toml.load(
            Path("src/config/").joinpath(project_config_file)
        )

        # setup database access
        database_path = (
            Path(__file__).parent.parent.parent.resolve()
            / "database"
            / database_name
        )
        self.con = sqlite3.connect(database_path)
        self.logger.info("Database loaded")

        ### prep objects ###
        # DF
        # Index | CommitID | CommitHash | CommitTag
        self.commitIDs, self.commits_df = self.__load_commits_dataframe()
        # Index | TestID | TestName |TestPath
        (
            self.testsuite_novar,
            self.testsuite_df,
        ) = self.__load_testsuite_dataframe()

        # Index | CommitID | File | Change
        self.filediff_df = self.__load_filediff_dataframe()
        # Index | CommitID | TestName | Result
        self.testrun_df = self.__load_testrun_dataframe()

    ############################
    ##### INITIALIZATION
    ############################
    def __load_commits_dataframe(self) -> tuple[list[str], pd.DataFrame]:
        commits_df = self.__get_database_table_as_df("Commits")
        commits_df = commits_df.astype(
            {"CommitID": int, "CommitHash": "string", "CommitTag": "string"}
        )
        commit_ids = [row[1]["CommitID"] for row in commits_df.iterrows()]

        return commit_ids, commits_df

    def __load_testsuite_dataframe(self) -> tuple[list[str], pd.DataFrame]:
        testsuite_df = self.__get_database_table_as_df("TestSuite")
        testsuite_df = testsuite_df.astype(
            {"TestID": int, "TestName": "string", "TestPath": "string"}
        )
        testsuite_without_variation = {
            re.sub(r"\d+", "", elem)
            for elem in testsuite_df["TestName"].to_list()
        }

        return sorted(testsuite_without_variation), testsuite_df

    def __load_filediff_dataframe(self) -> pd.DataFrame:
        filediff_df = self.__get_database_table_as_df("FileDiff")
        filediff_df = filediff_df.astype(
            {"CommitID": int, "File": "string", "Change": "string"}
        )
        return filediff_df

    def __load_testrun_dataframe(self) -> pd.DataFrame:
        testrun_df = self.__get_database_table_as_df("TestRun")
        testrun_df = testrun_df.astype(
            {
                "CommitID": int,
                "TestName": "string",
                "Passed": int,
                "Failed": int,
                "Skipped": int,
            }
        )
        return testrun_df

    ############################
    ##### DATA MANIPULATIONS
    ############################
    def get_filechange_encoded(self, df_filediff) -> pd.DataFrame:
        """
        return Dataframe of filechange as bool encoded per commit and FileComplexity, AddedLines, DeletedLines of Commit

        CommID | File1 | File 2 | File 3 |...|
        10          0      0        1
        """
        self.logger.info("encode filechange")

        filediff_pcommit_list = [
            df_filediff.loc[
                (df_filediff["CommitID"] == commitID)
                & (
                    df_filediff["File"].str.contains(
                        fnmatch.translate(
                            self.repo_config["pytest"]["python_files"]
                        )
                    )
                    == False
                )
            ]["File"].to_list()
            for commitID in self.commitIDs
        ]

        # encode filediff per commit with 0 & 1
        mlb = MultiLabelBinarizer()
        return pd.DataFrame(
            mlb.fit_transform(filediff_pcommit_list), columns=mlb.classes_
        )

    def get_filechange_metadata_encoded(
        self, df_filediff, complexity_filter
    ) -> pd.DataFrame:
        """
        return Dataframe of filechange as bool encoded per commit and FileComplexity, AddedLines, DeletedLines of Commit

        CommID | File1 | File 2 | File 3 |...| FileComplexity | AddedLines | DeletedLines
        10          0      0        1
        """
        self.logger.info("encode filechange")

        filediff_pcommit_list = [
            df_filediff.loc[
                (df_filediff["CommitID"] == commitID)
                & (
                    df_filediff["File"].str.contains(
                        fnmatch.translate(
                            self.repo_config["pytest"]["python_files"]
                        )
                    )
                    == False
                )
                & (df_filediff["FileComplexity"] >= complexity_filter)
            ]["File"].to_list()
            for commitID in self.commitIDs
        ]

        # encode filediff per commit with 0 & 1
        mlb = MultiLabelBinarizer()
        filediff_binarized = pd.DataFrame(
            mlb.fit_transform(filediff_pcommit_list), columns=mlb.classes_
        )

        # add metadata of repository to dataframe
        meta_data_pcomit = {
            "FileComplexity": [],
            "AddedLines": [],
            "DeletedLines": [],
        }
        for commitID in self.commitIDs:
            temp_df = df_filediff.loc[
                (df_filediff["CommitID"] == commitID)
                & (
                    df_filediff["File"].str.contains(
                        fnmatch.translate(
                            self.repo_config["pytest"]["python_files"]
                        )
                    )
                    == False
                )
                & (df_filediff["FileComplexity"] >= complexity_filter),
                ["FileComplexity", "AddedLines", "DeletedLines"],
            ]

            meta_data_pcomit["FileComplexity"].append(
                sum(temp_df["FileComplexity"].to_list())
            )
            meta_data_pcomit["AddedLines"].append(
                sum(temp_df["AddedLines"].to_list())
            )
            meta_data_pcomit["DeletedLines"].append(
                sum(temp_df["DeletedLines"].to_list())
            )

        return pd.concat(
            [filediff_binarized, pd.DataFrame.from_dict(meta_data_pcomit)],
            axis=1,
            join="inner",
        )

    def prep_dataframe_testrun(
        self,
        df_testrun: pd.DataFrame,
        filechange_encoded: pd.DataFrame,
    ):
        """
        one hot encode testfiles in testrun_df
        add filechange as bool val to testrun_df
        """
        self.logger.info("prepare dataframe testrun")

        ### one hot encode testfiles
        testfile_dummies = pd.get_dummies(df_testrun["TestName"])
        # drop TestName
        testrun_drop = df_testrun.drop(columns="TestName")
        # join
        testrun_app = testrun_drop.join(testfile_dummies)
        filechange_matching_testrun = []
        for commitID in tqdm(df_testrun["CommitID"].to_list()):
            # create list of filediff according to commitID -> iloc = index
            filechange_matching_testrun += filechange_encoded.iloc[
                [commitID - 1]
            ].values.tolist()
        self.logger.info("finishing dataframe testrun")
        filechange_streched_df = pd.DataFrame(
            filechange_matching_testrun, columns=filechange_encoded.columns
        )
        testrun_app = testrun_app.reset_index(drop=True)
        return testrun_app.join(filechange_streched_df)

    def normalize_testrun_column(
        self, df_testrun: pd.DataFrame, column_names: list[str]
    ) -> pd.DataFrame:
        """
        normalize a column to 0 and >0
        """
        self.logger.info("normalize testrun results to 0 | 1")
        df_temp = df_testrun.copy(deep=True)
        for elem in column_names:
            df_temp[elem] = np.where(df_testrun[elem] > 0, 1, 0)

        df_temp["PassFail"] = np.where(df_testrun["Failed"] > 0, 0, 1)
        return df_temp

    def split_learning_testing_data(
        self, df_testrun_normalized: pd.DataFrame
    ) -> tuple[pd.DataFrame, pd.DataFrame]:
        """
        return the testing_df and learning_df of normalized and prepared testrun_df
        """
        # first 10% of commits from repsitory are used for testing,
        # next 90% of commits from repositry are used for training
        testdata_percent = 10
        self.logger.info(
            f"split data in learning[{100-testdata_percent}%]|testing[{testdata_percent}%]"
        )
        df_commitIDs = df_testrun_normalized["CommitID"].tolist()
        commit_sum = max(df_commitIDs) - min(df_commitIDs)
        repo_separation_size = max(df_commitIDs) - (
            commit_sum / testdata_percent
        )
        testing_df = df_testrun_normalized.loc[
            df_testrun_normalized["CommitID"] > repo_separation_size
        ]
        training_df = df_testrun_normalized.loc[
            df_testrun_normalized["CommitID"] <= repo_separation_size
        ]

        # reset index in tests
        training_df = training_df.reset_index(drop=True)
        testing_df = testing_df.reset_index(drop=True)
        return testing_df, training_df

    def split_features_and_label(
        self, label_col: str, dataframe: pd.DataFrame
    ) -> tuple[np.array, np.array]:
        """
        separate a dataframe into labels and features
            -lables: things you want to predict
            -featurs: data used for prediction
        """
        labels_df = dataframe[label_col]
        features_df = dataframe.drop(columns=label_col)
        return labels_df, features_df

    ############################
    ##### DATA CALCULATIONS
    ############################
    def calc_passfail_test_percommit(
        self, testrun_df: pd.DataFrame, commit_ids: list[str]
    ) -> None:
        """
        return a dataframe with summarizing all testresults per commit for testrun_df
        """
        # containing sum of tests
        sum_failtest_per_commit = []
        sum_passtest_per_commit = []
        # containing filenames
        failedtest_per_commit = []
        passedtest_per_commit = []

        for commit_id in tqdm(commit_ids):

            failed_tests_df = testrun_df.loc[
                (testrun_df["CommitID"].isin([commit_id]))
                & (testrun_df["Failed"] > 0)
            ]
            failedtest_per_commit.append(list(failed_tests_df["TestName"]))
            sum_failtest_per_commit.append(len(failed_tests_df))

            passed_tests_df = testrun_df.loc[
                (testrun_df["CommitID"].isin([commit_id]))
                & (testrun_df["Passed"] > 0)
                & (testrun_df["Failed"] == 0)
            ]
            passedtest_per_commit.append(list(passed_tests_df["TestName"]))
            sum_passtest_per_commit.append(len(passed_tests_df))

        return pd.DataFrame(
            {
                "CommitID": commit_ids,
                "SumFailed": sum_failtest_per_commit,
                "SumPassed": sum_passtest_per_commit,
                "FailedTests": failedtest_per_commit,
                "PassedTests": passedtest_per_commit,
            }
        )

    def calc_testchange_pcommit(
        self, testresult_pcommit_df: pd.DataFrame
    ) -> pd.DataFrame:
        """
        calc testchange from commit compared to previous commit
        return two lists:
            - testchange per commit
            - filechange per commit
        """
        sum_testchange_pcommit = (
            []
        )  # tests that changed behavior compared to last commit
        file_testchange_pcommit = (
            []
        )  # tests that changed behavior compared to last commit
        passedtest_per_commit = testresult_pcommit_df["PassedTests"].to_list()
        for index, tests in enumerate(passedtest_per_commit):
            if index == 0:
                sum_testchange_pcommit.append(0)
                file_testchange_pcommit.append(0)
                continue
            # check difference in passing tests between commits with
            # .symmetric_difference of two sets containing tests from two commits
            test_change = set(tests).symmetric_difference(
                set(passedtest_per_commit[index - 1])
            )
            if len(test_change):  # true when set is NOT empty
                file_testchange_pcommit.append(list(test_change))
            else:
                file_testchange_pcommit.append(0)
            sum_testchange_pcommit.append(len(test_change))
        return pd.DataFrame(
            {
                "CommitID": self.commitIDs,
                "SumTestChange": sum_testchange_pcommit,
                "TestChange": file_testchange_pcommit,
                "CommSize": self.calc_commitsize_pcommit(
                    self.filediff_df, self.commitIDs
                ),
            }
        )

    def calc_commitsize_pcommit(
        self, filediff_df: pd.DataFrame, commit_IDs: list[str]
    ) -> list[int]:
        """
        return list of commit size per commit
            list[0]=Commit_1
        """
        return [
            len(filediff_df.loc[(filediff_df["CommitID"].isin([commit_id]))])
            for commit_id in commit_IDs
        ]

    def calc_commitID_pfilechange(self, filediff_df: pd.DataFrame) -> None:
        """
        dict[ filename : list[ CommitIDs ] ]
        get all filenames of all python files that got modified/added in repo
        add commit-IDs of filechange to dict
        """
        files_that_change = {
            row[1]["File"]
            for row in filediff_df.iterrows()
            if row[1]["Change"] in ["MODIFY", "ADD", "RENAME"]
            and ".py" in row[1]["File"]
            and "test" not in row[1]["File"]
        }

        # dict{filename:Commits in which file changes}
        return {
            file: filediff_df.loc[filediff_df["File"] == file, ["CommitID"]][
                "CommitID"
            ].to_list()
            for file in files_that_change
        }

    def calc_testchange_pfile(
        self,
        files_with_commid: dict[str, list[int]],
        testresult_pcommit_df: pd.DataFrame,
    ) -> dict[str, pd.DataFrame]:
        """
        dict[ filename : DataFrame(CommitIDs | TestChange | CommSize)]
        create dict of filename and dataframe of changes
        """
        files_with_testchange_pcommit = {}
        for __filename, commit_ids in tqdm(files_with_commid.items()):
            test_changes = []
            commit_size = []
            for commit_id in commit_ids:
                test_changes.append(
                    testresult_pcommit_df.iloc[commit_id - 1]["SumTestChange"]
                )
                commit_size.append(
                    testresult_pcommit_df.iloc[commit_id - 1]["CommSize"]
                )
            temp_df = pd.DataFrame(
                data={
                    "CommitID": commit_ids,
                    "SumTestChange": test_changes,
                    "CommSize": commit_size,
                }
            )
            files_with_testchange_pcommit[__filename] = temp_df
        return files_with_testchange_pcommit

    def calc_testchange_file_and_test(
        self,
        testsuite_df: pd.DataFrame,
        files_with_commid: dict[str : list[int]],
        df_testresult_pcommit: pd.DataFrame,
    ) -> pd.DataFrame:
        """
        calculate the testchange per file for whole repository
        return dataframe with:
            columnnames = files in repo
            index = tests in testsuite
            fields = sum of testschange over whole repository
        """
        df_files_with_tests = pd.DataFrame(
            0,
            index=testsuite_df["TestName"].to_list(),
            columns=sorted(list(files_with_commid.keys())),
        )
        df_files_with_tests.columns.name = "Files"
        df_files_with_tests.index.name = "Tests"
        for filename, commitIDs in tqdm(files_with_commid.items()):
            testchange_per_file = []
            for commit in commitIDs:
                testchange_in_commit = df_testresult_pcommit.loc[
                    df_testresult_pcommit["CommitID"] == commit
                ]["TestChange"].values
                if testchange_in_commit[0] != 0:
                    testchange_per_file += testchange_in_commit[0]
            for testname, occurences in Counter(testchange_per_file).items():
                df_files_with_tests[filename][testname] = occurences

        return df_files_with_tests

    def calc_testchange_file_and_test_novar(
        self,
        testsuite_without_variation: list[str],
        files_with_commid: dict[str, list[int]],
        files_with_tests_df: pd.DataFrame,
    ) -> dict:
        """calculate the testchange per file for all tests without variation

        Args:
            testsuite_without_variation (list[str]):
            files_with_commid (dict[str, list[int]]):
            files_with_tests_df (pd.DataFrame):

        Returns:
            dict
        """
        df_files_with_tests_novar = pd.DataFrame(
            0,
            index=testsuite_without_variation,
            columns=sorted(list(files_with_commid.keys())),
        )

        data_stacked = pd.DataFrame(
            files_with_tests_df.stack(), columns=["TestOccur"]
        ).reset_index()
        df_files_with_tests_novar.columns.name = "Files"
        df_files_with_tests_novar.index.name = "Tests"

        for index in tqdm(data_stacked.index):
            # choose best match of testvariartion in testsuite_without_variation
            origin_test = process.extractOne(
                data_stacked["Tests"][index],
                testsuite_without_variation,
                scorer=fuzz.ratio,
            )

            # add TestOccurence value for test and file from data_heat to new df
            df_files_with_tests_novar.loc[origin_test[0]][
                data_stacked["Files"][index]
            ] += data_stacked["TestOccur"][index]
        return df_files_with_tests_novar

    ############################
    ##### PRIVATE
    ############################
    def __get_database_table_as_df(self, tablename: str) -> pd.DataFrame:
        return pd.read_sql_query(f"SELECT * from {tablename}", self.con)

    def __select_testresults_for_all_commits(
        self, df_single_test: DataFrame, commitIDs
    ) -> DataFrame:
        result_dict = {
            "CommitID": [],
            "TestName": [],
            "Passed": [],
            "Failed": [],
            "Skipped": [],
        }
        for commit in commitIDs:
            temp_df = (
                df_single_test.loc[df_single_test["CommitID"] == commit]
                .sort_values("TestName", ascending=False, key=natsort_keygen())
                .reset_index(drop=True)
            )

            # some commits have been skipped, so test_df can be empty
            if temp_df.empty:
                continue

            temp_dict = self.__choose_matching_value(
                list(temp_df.itertuples(name=None))
            )

            result_dict["CommitID"].append(temp_dict["CommitID"])
            result_dict["TestName"].append(
                temp_dict["TestName"].translate(remove_digits)
            )
            result_dict["Passed"].append(temp_dict["Passed"])
            result_dict["Failed"].append(temp_dict["Failed"])
            result_dict["Skipped"].append(temp_dict["Skipped"])
        return DataFrame.from_dict(result_dict)

    def __choose_matching_value(
        self, tuple_list: list[tuple[int, int, str, int, int, int]]
    ) -> testresult_row:
        for elem in tuple_list:

            if int(re.findall(r"\d+", elem[2])[0]) >= elem[1]:
                if elem != tuple_list[-1]:
                    continue

            if (elem == tuple_list[-1]) or (
                int(re.findall(r"\d+", elem[2])[0]) < elem[1]
            ):
                mydict: testresult_row = {
                    "CommitID": int(elem[1]),
                    "TestName": re.sub(r"", "", elem[2]),
                    "Passed": int(elem[3]),
                    "Failed": int(elem[4]),
                    "Skipped": int(elem[5]),
                }
                return mydict

    ############################
    ##### MACHINE LEARNING
    ############################
    def create_training_testing_batches(
        self,
        testrun_input: DataFrame,
        complexity_filter: int,
        startcom_tagver: str,
        drop_commitID: bool = True,
        label_column: str = "PassFail",
        drop_metadata: list[str] = None,
    ) -> tuple:
        """separate the dataset in training and testing batches.
        it is possible to set all options for creating the dataset

        Args:
            testrun_input (DataFrame):
            complexity_filter (int): file complexity to filter
            startcom_tagver (str): git tag to which start from
            drop_commitID (bool, optional): drop the commit-ID as feature in dataset. Defaults to True.
            label_column (str, optional): label feature. Defaults to "PassFail".

        Returns:
            tuple:
        """
        ### add filechange as boolc
        if complexity_filter is None:
            filechange_encoded: pd.DataFrame = self.get_filechange_encoded(
                self.filediff_df
            )
        else:
            filechange_encoded: pd.DataFrame = (
                self.get_filechange_metadata_encoded(
                    self.filediff_df, complexity_filter
                )
            )
        # combine testrun with one hot encoded filediffs and metadata
        df_testrun = self.prep_dataframe_testrun(
            testrun_input, filechange_encoded
        )

        # drop testrun before selected commit tag
        if startcom_tagver:
            # cut out commits to tag set in startcom_tagver
            commitID_for_tag = self.commits_df.loc[
                self.commits_df["CommitTag"] == str(startcom_tagver)
            ]["CommitID"].values
            df_testrun = df_testrun.drop(
                df_testrun[df_testrun["CommitID"] < commitID_for_tag[0]].index
            )

        # set all values in "PassedFail" = 0 if "Failed" =1, or 1 if "Failed" = 0
        df_testrun = self.normalize_testrun_column(
            df_testrun, ["Passed", "Failed", "Skipped"]
        )
        df_testrun = df_testrun.drop(columns=["Skipped", "Failed", "Passed"])

        if drop_metadata:
            df_testrun = df_testrun.drop(columns=drop_metadata)

        testing_df, training_df = self.split_learning_testing_data(df_testrun)

        if drop_commitID:
            testing_df = testing_df.drop(columns="CommitID")
            training_df = training_df.drop(columns="CommitID")

        train_label_df, train_feat_df = self.split_features_and_label(
            label_column, training_df
        )
        test_label_df, test_feat_df = self.split_features_and_label(
            label_column, testing_df
        )

        self.logger.info(
            f"\n Training Features Shape: {train_feat_df.shape}"
            + f"\n Training Labels Shape: {train_label_df.shape}"
            + f"\n Testing Features Shape: {test_feat_df.shape}"
            + f"\n Testing Labels Shape: {test_label_df.shape}"
        )
        return (
            df_testrun,
            train_feat_df,
            train_label_df,
            test_feat_df,
            test_label_df,
        )

    def combine_testresult_tonovar(
        self, testsuite_novar: list[str], testresults_df: DataFrame
    ) -> DataFrame:
        """create a dataframe with no testvariations of testresults.
        Idea: using testvariation after commit it got created
        testnumber > commitID

        Args:
            testsuite_novar (list[str]): all tests in testsuite without variations
            testresults_df (DataFrame): testresults from database
        Returns:
            DataFrame: dataframe of testresults without variations
        """
        novar_testresults: list[DataFrame] = []
        for test in tqdm(testsuite_novar):
            df_testisolated = testresults_df.loc[
                testresults_df["TestName"].str.contains(
                    test.strip(".py") + r"\d*.py"
                )
            ]
            novar_testresults.append(
                self.__select_testresults_for_all_commits(
                    df_testisolated, self.commitIDs
                )
            )
        return pd.concat(novar_testresults, ignore_index=True).sort_values(
            by=["CommitID", "TestName"], ignore_index=True
        )
