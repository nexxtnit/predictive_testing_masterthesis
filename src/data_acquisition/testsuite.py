import configparser

from pathlib import Path
import shutil

from typing import List, MutableMapping, Union

from pydriller.domain.commit import Commit

from global_tools import system
from global_tools.config import global_config as config
from loguru import logger
from tqdm import tqdm

from src.data_acquisition.database import DataBaseTest
from src.data_acquisition.git_tools import ProjectRepository

from thefuzz import fuzz, process
from collections import Counter
import multiprocessing as mp
import numpy as np

import re
import fnmatch


class TestSuite:
    def __init__(self, repo_config_obj: MutableMapping):
        # init logger
        self.logger = logger.bind(machine="host")
        self.logger = self.logger.bind(TS=True)

        # init project repository object
        self.repo_config = repo_config_obj
        self._repo = ProjectRepository(
            repo_config_obj["git-info"]["ssh_address"],
            repo_config_obj["git-info"]["start_commit"],
        )
        self.commit_objs: list[Commit] = []
        self.commits: list[str] = []

        # INIT REPO
        self.init_repository()

        # init database object
        self.database = DataBaseTest(config)
        # INIT DATABASE
        self.init_database()

        # init test suite
        self.testsuite_dir: Path = (
            Path.cwd() / config["build"]["path"] / config["testsuite"]["path"]
        )
        self.testsuite_dir.mkdir(parents=True, exist_ok=True)

        self.temp_dir: Path = (
            Path.cwd() / config["build"]["path"] / config["temp"]["path"]
        )

        self.pytest_ini_path = self._repo.directory / "pytest.ini"
        # testsuite
        self.test_files: set[tuple[str, str]] = set()
        self.test_names: list[str] = []
        self.test_paths: Union[list[str], list[Path]] = []
        self.tests_to_skip: list[str] = self.repo_config["testrun"][
            "tests_to_skip"
        ]
        # Pytest Init
        self.repo_config["pytest"]["addopts"] += (
            " --basetemp=" + self.temp_dir.__str__()
        )
        # INIT TESTSUITE
        self.init_testsuite()

    ############################
    ##### INITIALIZATIONS
    ############################
    def init_repository(self):
        """
        initialize the repository in build dir for analysis
        """
        # true if repository dir is empty
        self.logger.info("initialize repository")
        if not any(self._repo.directory.iterdir()):
            self._repo.download_repo()
        self._repo.config_git()
        self.restore_repo_to_start_commit()
        self._repo.create_repository_object(self._repo.directory.__str__())
        self.commit_objs = self._repo.get_commit_objs()

        for commit in list(self.commit_objs):
            if commit.hash == self.repo_config["git-info"]["stop_commit"]:
                break
            self.commit_objs.remove(commit)
        self.commits = [commit.hash for commit in self.commit_objs]
        self.logger.info("repository initialized")

    def init_database(self):
        """
        initialize database with data from repository
        """
        self.logger.info("initializing database")
        self.database.initialize_database()

        # true if last commit NOT in database yet
        if not self.database.check_commit_exists(self.commits[-1]):
            tags = self._repo.get_tags(self.commits)
            self.database.insert_commits(self.commits, tags)

        # get last commit that is no merge
        for commit in reversed(self.commit_objs):
            if not commit.merge:
                lastcommit_nomerge = commit.hash
                break
        # check if last commit exists in database
        if not self.database.get_all_diff_of_commit(lastcommit_nomerge):
            # fill file diffs per commit into database
            self.logger.info(
                "initialize table <FileDiff> with diff data by commit from git repository"
            )
            # iter over all commits
            self.database.open_database()

            for commit in tqdm(self.commit_objs):
                for file in commit.modified_files:
                    # separate changed files and insert into separate database entries
                    filename = file.filename
                    filepath = file.new_path
                    change_type = file.change_type.name
                    complexity = int(file.complexity or 0)
                    added_lines = int(file.added_lines or 0)
                    deleted_lines = int(file.deleted_lines or 0)
                    lines_of_code = int(file.nloc or 0)
                    self.database.insert_new_diff(
                        commit.hash,
                        filename,
                        change_type,
                        complexity,
                        added_lines,
                        deleted_lines,
                        lines_of_code,
                        filepath,
                    )
            self.database.close_database()
            self.restore_repo_to_start_commit()
        self.logger.info("database initialized")

    ############################
    ##### initialize_database
    ############################

    def update_testfiles_from_filediff(self):
        """search for all files having test file pattern in them
        """
        self.test_files = set(
            self.database.get_all_testnames_and_paths_from_filediff()
        )

        for testcouple in list(self.test_files):

            if (
                self.repo_config["testrun"]["testsuitepath"]
                not in testcouple[1]
            ) or not (
                re.search(
                    f"/{fnmatch.translate(self.repo_config['pytest']['python_files'])}$",
                    testcouple[1],
                )
            ):
                print(testcouple[1])
                self.test_files.remove(testcouple)

        self.test_names: list[str] = [
            testcouple[0] for testcouple in self.test_files
        ]
        self.test_paths: Union[list[str], list[Path]] = [
            testcouple[1] for testcouple in self.test_files
        ]

    def update_testfiles_from_testsuite(self):
        """check in folder build/testsuite if tests are in there
        """
        self.test_files.clear()
        for testpath in self.testsuite_dir.rglob("*.py"):
            self.test_files.add(
                (testpath.name, testpath.relative_to(self.testsuite_dir))
            )
        self.test_names: list[str] = [
            testcouple[0] for testcouple in self.test_files
        ]
        self.test_paths: Union[list[str], list[Path]] = [
            testcouple[1] for testcouple in self.test_files
        ]

    def init_testsuite(self) -> None:
        """
        create a testsuite containing all test variations from all commits
        """

        self.logger.info("init testsuite")
        self.update_testfiles_from_testsuite()
        if not self.check_testsuite_exists_in_folder():
            self.logger.info("tests not in testsuite folder")
            self.restore_repo_to_start_commit()
            self.database.drop_table("TestSuite")
            self.database.initialize_database()
            self.update_testfiles_from_filediff()
            # extract all test variations to testsuite_dir
            self.logger.info("collect all testvariations from all commits")
            self.database.open_database()
            for testpath in tqdm(self.test_paths):
                commitIDs = self.database.get_all_commits_of_filepath(testpath)
                for commitID in commitIDs:
                    (
                        new_testname,
                        rel_test_path,
                    ) = self.copy_test_variation_to_testsuite(
                        self.database.get_commithash_of_commitid(commitID),
                        Path(testpath),
                    )
                    self.database.insert_tests_to_testsuite(
                        new_testname, rel_test_path, commitID
                    )
            self.update_testfiles_from_testsuite()
            self.database.close_database()
            self.logger.info("testsuite created")
        self.logger.info("testsuite complete")

    def install_test_dependencies(self):
        """
        installs test_dependencies from .toml file
        """
        self.logger.info("install test dependencies into venv")
        self.__cmd_run(
            ["pipenv", "install", "--skip-lock", "--dev"]
            + self.repo_config["testrun"]["test_dependencies"]
        )

    def remove_test_dependencies(self):
        """
        remove test_dependencies from .toml file
        """
        self.logger.info("remove test dependencies from venv")
        self.__cmd_run(
            ["pipenv", "uninstall"]
            + self.repo_config["testrun"]["test_dependencies"]
        )

    def check_testsuite_exists_in_folder(self) -> bool:
        """
        check if all tests from database are present in testsuite folder
        """
        if 0 in (
            len(self.test_names),
            len(list(self.testsuite_dir.rglob("*.py"))),
        ):
            return False

        return len(self.test_names) == len(
            list(self.testsuite_dir.rglob("*.py"))
        )

    def copy_test_variation_to_testsuite(
        self, commit_hash: str, test_path: Path
    ) -> tuple[str, str]:
        """
        copy a testvariation, located in a certain commit, to the testsuite
        return testname and testpath
        """
        self._repo.checkout_commit(commit_hash)
        commit_id = self.database.get_commitid_of_commithash(commit_hash)
        testname_old = test_path.stem
        test_name_new = f"{test_path.parent}/{testname_old}_{commit_id}.py"

        dest_path = self.testsuite_dir / test_name_new
        self.__copy_from_to(self._repo.directory.joinpath(test_path), dest_path)
        return Path(test_name_new).name, test_name_new

    ############################
    ##### TESTRUN
    ############################
    def skip_commit(self, commit_hash: str) -> bool:
        """skip commit if it is in config to skip"""
        commit_id = self.database.get_commitid_of_commithash(commit_hash)
        if str(commit_id) in list(
            self.repo_config["testrun"]["commits_to_skip"]
        ):
            self.logger.info(
                f"Commit [{commit_id}] is in skip list, skipping commit"
            )
            return True
        return False

    def copy_testsuite_to_repo(self) -> None:
        """
        copy the whole testsuite into the repository directory
        """

        src = self.testsuite_dir
        dst = self._repo.directory
        dst_testspath: Path = dst / self.repo_config["testrun"]["testsuitepath"]

        if dst_testspath.is_dir:
            for file in dst_testspath.rglob(
                self.repo_config["pytest"]["python_files"]
            ):
                if file.is_file():
                    file.unlink()

        for test in self.test_paths:
            if any(
                testskip in test.__str__() for testskip in self.tests_to_skip
            ):
                continue
            if not (dst / test.parent).is_dir():
                (dst / test.parent).mkdir(parents=True, exist_ok=True)
            shutil.copy2(src / test, dst / test)

    def create_pytest_ini(self):
        """
        creates pytest.ini file according to conf.toml in project repo directory
        """
        self.logger.info("create pytest.ini in repo path")
        with open(self.pytest_ini_path, "w") as pytest_file:
            pytest_ini = configparser.ConfigParser()
            pytest_ini["pytest"] = self.repo_config["pytest"]
            pytest_ini.write(pytest_file)

    def run_testsuite(self, commit_hash: str):
        """
        run the testcases for a certain commit
        """
        self.temp_dir.mkdir(exist_ok=True)

        commit_id = self.database.get_commitid_of_commithash(commit_hash)
        self.logger.info(
            f"start testsuite to commit[{commit_id}/{len(self.commits)}][{commit_hash}]"
        )
        self.restore_repo_to_start_commit()

        self._repo.checkout_commit(commit_hash)

        # copy testsuite to repo directory
        self.logger.info("copy testsuite to repo folder")
        self.update_testfiles_from_testsuite()
        self.copy_testsuite_to_repo()

        self.create_pytest_ini()

        # install local package
        self.logger.info("install locale package")
        out = self.__cmd_run(
            [
                "pipenv",
                "run",
                "pip",
                "install",
                ".",
                "--use-feature=in-tree-build",
            ],
            self._repo.directory,
        )
        for element in out:
            if "ERROR" in element:
                self.logger.error(
                    "Error in pip install -e, skipping this commit"
                )
                return

        # run pytest
        self.logger.info("run testsuite")
        output: list[str] = self.__cmd_run(
            [
                "pipenv",
                "run",
                "pytest",
            ],
            self._repo.directory,
            no_output=True,
        )
        self.logger.debug("\n".join(output))

        # uninstall local package
        self.__cmd_run(
            [
                "pipenv",
                "run",
                "pip",
                "uninstall",
                "-y",
                self.repo_config["name"],
            ],
            self._repo.directory,
        )

        # collect done tests from output of pytest
        (
            tests_skipped,
            tests_failed,
            tests_passed,
        ) = self.multiprocess_output_analysis(output)

        all_tests_set = set(tests_skipped + tests_passed + tests_failed)
        # log passed and failed tests
        self.logger.bind(TP=True).debug(
            "TESTS SKIPPED" + "\n".join(tests_skipped)
        )
        self.logger.bind(TP=True).info(
            f"TESTS SKIPPED [{len(set(tests_skipped))}]/[{len(all_tests_set)}] of Commit[{commit_id}]"
        )
        self.logger.bind(TF=True).debug(
            "TESTS FAILED" + "\n".join(tests_failed)
        )
        self.logger.bind(TF=True).info(
            f"TESTS FAILED [{len(set(tests_failed))}]/[{len(all_tests_set)}] of Commit[{commit_id}]"
        )
        self.logger.bind(TP=True).debug(
            "TESTS PASSED" + "\n".join(tests_passed)
        )
        self.logger.bind(TP=True).info(
            f"TESTS PASSED [{len(set(tests_passed)-set(tests_failed))}]/[{len(all_tests_set)}] of Commit[{commit_id}]"
        )

        # tests_done needs to be equal len(self.test_names)

        testdifference = {
            testpath.__str__() for testpath in self.test_paths
        }.difference(all_tests_set)

        if list(testdifference):
            self.logger.info(
                f"testdifference, tests in testsuite Vs. tests collected:\n {testdifference}"
            )

        tests_done = len(all_tests_set) + len(self.tests_to_skip)

        if tests_done == len(self.test_names):
            self.logger.bind(TP=True).success(
                f"Tests done[{tests_done}] of [{len(self.test_names)}]"
            )
        else:
            self.logger.bind(TF=True).error(
                f"Tests done[{tests_done}] of [{len(self.test_names)}]"
            )
            # if testrun not successfull, repeat
            self.run_testsuite(commit_hash)

        # write to database
        self.logger.info("write test run into database")
        testspass_cnt = Counter(tests_passed)
        testsfail_cnt = Counter(tests_failed)
        testsskip_cnt = Counter(tests_skipped)
        self.database.open_database()
        for i in range(len(self.test_paths)):
            testname = self.test_names[i]
            self.database.insert_new_testresult(
                commit_id,
                testname,
                testspass_cnt.get(str(self.test_paths[i]), 0),
                testsfail_cnt.get(str(self.test_paths[i]), 0),
                testsskip_cnt.get(str(self.test_paths[i]), 0),
            )
        self.database.close_database()

        shutil.rmtree(self.temp_dir)

    def check_if_commit_tested(self, commit_hash: str) -> bool:
        """check in database if commit is already tested

        Args:
            commit_hash (str)

        Returns:
            bool
        """
        commitID = self.database.get_commitid_of_commithash(commit_hash)
        database_tests = self.database.get_testresult_for_commit(commitID)

        if database_tests is None:
            return False
        if len(database_tests) == len(self.test_names):
            self.logger.info(
                f"testrun for commit [{commitID}][{commit_hash}] already in database"
            )
            return True
        self.logger.error(
            f"testrun length in database={len(database_tests)}, testrun length should be:{len(self.test_names)}\n"
            + f"Drop testrun for commit[{commitID}] rerun test for commit[{commitID}]"
        )
        self.database.drop_testrun_by_commit(commitID)
        return False

    def sort_testrun_output(
        self, queue: mp.Queue, testrun_output: list[str]
    ) -> None:
        """
        sort the output list into skipped, faileda and passed tests.
        Identify the tests according to the testnames
        """

        tests_skipped = []
        tests_failed = []
        tests_passed = []

        testpathnames = [
            testpathname.__str__() for testpathname in self.test_paths
        ]
        for line in tqdm(testrun_output):
            if line.startswith(("PASSED", "ERROR", "FAILED", "SKIPPED")):
                # check for testpaths because they are larger strings, less possibilities,quicker match
                match = process.extractOne(
                    line,
                    testpathnames,
                    scorer=fuzz.partial_ratio,
                )
                if match is not None:
                    if line.startswith("SKIPPED"):
                        tests_skipped.append(match[0])
                    if line.startswith("PASSED"):
                        tests_passed.append(match[0])
                    if line.startswith(("ERROR", "FAILED")):
                        tests_failed.append(match[0])

        queue.put([tests_skipped, tests_failed, tests_passed])

    def multiprocess_output_analysis(
        self, testrun_output: list[str]
    ) -> tuple[list[str], list[str], list[str]]:
        """
        analyses testrun output in multiple processes
        """
        self.logger.info("analyse testrun output")
        process_cnt = mp.cpu_count() - 1
        output_split = np.array_split(testrun_output, process_cnt)
        queue = mp.Queue()
        processes: list[mp.Process] = []

        for i in range(process_cnt):
            p = mp.Process(
                target=self.sort_testrun_output, args=(queue, output_split[i])
            )
            processes.append(p)
            p.start()

        tests_skipped = []
        tests_failed = []
        tests_passed = []
        for p in processes:
            result = queue.get()
            tests_skipped += result[0]
            tests_failed += result[1]
            tests_passed += result[2]

        for p in processes:
            p.join()

        return tests_skipped, tests_failed, tests_passed

    def restore_repo_to_start_commit(self):
        """reset repo to first commit
        """
        self._repo.checkout_commit(self._repo.start_commit)
        self._repo.git_cmd_run(["reset", "--hard", "HEAD"], no_output=True)
        self._repo.git_cmd_run(["clean", "-fxd"], no_output=True)

    def finish_testrun(self):
        """copy database to database folder and finish testrun
        """
        self.logger.info("testsuite finishing")
        database_store_path: Path = (
            self.database.storage_path / self.repo_config["database"]["file"]
        )
        if not database_store_path.is_file():
            self.logger.info(
                f"copying test_data.db to /database/{self.repo_config['database']['file']}"
            )
            shutil.copy2(self.database.database_path, database_store_path)

    ############################
    ##### PRIVATE FUNCTIONS
    ############################

    def __copy_from_to(
        self, source_file_path: Path, dest_file_path: Path
    ) -> None:
        """
        copy a testfile to the testsuite folder and create all subfolders if != exists
        """
        self.logger.debug(
            f"copy file[{source_file_path}] to [{dest_file_path}]"
        )
        dest_file_path.parent.mkdir(parents=True, exist_ok=True)
        if not dest_file_path.is_file():
            shutil.copy2(source_file_path, dest_file_path)
        return

    def __cmd_run(
        self, command: list[str], exec_location: str = None, no_output=False
    ) -> list[str]:
        """
        run command with own logger
        """
        self.logger.opt(depth=2).info("'" + " ".join(command) + "' :")
        return system.cmd_run(command, self.logger, exec_location, no_output)
