#!/usr/bin/env python3
"""
Description:

"""

from pathlib import Path
import toml
from loguru import logger

from data_acquisition.git_tools import ProjectRepository
from global_tools.logger import global_logger as logger
from data_acquisition.database import DataBaseTest
from data_acquisition.testsuite import TestSuite
from global_tools.config import project_to_test

# init logger
dataAc_logger = logger.bind(machine="host")
dataAc_logger = dataAc_logger.bind(DA=True)


SOURCE_DIR = Path.cwd()
WORK_DIR = SOURCE_DIR / "build"
WORK_DIR.mkdir(parents=False, exist_ok=True)


testsuite = TestSuite(project_to_test)

testsuite.install_test_dependencies()
for commit in reversed(testsuite.commits):
    if not testsuite.check_if_commit_tested(commit):
        if not testsuite.skip_commit(commit):
            testsuite.run_testsuite(commit)
testsuite.finish_testrun()

testsuite.remove_test_dependencies()
