#!/usr/bin/env python
"""
Description:
    Global logger module
"""
from loguru import logger
import sys
from pathlib import Path
from .config import global_config

logger.remove()
global_logger = logger
config = global_config

### GLOBAL LOGGER ###
# cwd/build/logs
GLOBAL_LOG_PATH: Path = (
    Path(__file__).parent.parent.parent.resolve() / config["logger"]["path"]
)
GLOBAL_LOG_PATH.mkdir(parents=True, exist_ok=True)

# format logger
time_format = "<y>{time:YYYY-MM-DD HH:mm:ss.SSS}</y>"
level_format = "<level>{level: <8}</level>"
log_message_format = "<b>{extra[machine]}</b> > <cyan>{module}</cyan>:<cyan>{function}</cyan>:<y>{line}</y> - <w>{message}</w>"
global_logger.level("SUCCESS", color="<g>")

# add stdout logger
global_logger.add(
    sys.stdout,
    format=f"{time_format} | {level_format} | {log_message_format}",
    level=20,
)

### logfile global
logfile = GLOBAL_LOG_PATH / config["logger"]["global"]["path"]
open(logfile, "w").close()  # erase logfile content
global_logger.add(
    logfile,
    format=f"{time_format} | {level_format} | {log_message_format}",
    level=1,
)


### logfile data acquisition
logfile_da = GLOBAL_LOG_PATH / config["logger"]["data_acquisition"]["path"]
open(logfile_da, "w").close()  # erase logfile content
logger.add(
    logfile_da,
    level=1,
    filter=lambda record: "DA" in record["extra"],
    format=f"{time_format} | {level_format} | {log_message_format}",
)

### logfile testuite
logfile_ts = GLOBAL_LOG_PATH / config["logger"]["testsuite"]["path"]
open(logfile_ts, "w").close()  # erase logfile content
logger.add(
    logfile_ts,
    level=1,
    filter=lambda record: "TS" in record["extra"],
    format=f"{time_format} | {level_format} | {log_message_format}",
)

### logfile tests passed
logfile_tp = GLOBAL_LOG_PATH / config["logger"]["testsuite"]["test_passed"]
open(logfile_tp, "w").close()  # erase logfile content
logger.add(
    logfile_tp,
    level=1,
    filter=lambda record: "TP" in record["extra"],
    format=f"{time_format} | {level_format} | {log_message_format}",
)

### logfile tests failed
logfile_tf = GLOBAL_LOG_PATH / config["logger"]["testsuite"]["test_failed"]
open(logfile_tf, "w").close()  # erase logfile content
logger.add(
    logfile_tf,
    level=1,
    filter=lambda record: "TF" in record["extra"],
    format=f"{time_format} | {level_format} | {log_message_format}",
)

### logfile data visualization
logfile_dv = GLOBAL_LOG_PATH / config["logger"]["data_visualization"]["path"]
open(logfile_dv, "w").close()  # erase logfile content
logger.add(
    logfile_dv,
    level=1,
    filter=lambda record: "DV" in record["extra"],
    format=f"{time_format} | {level_format} | {log_message_format}",
)
### logfile data preparation
logfile_dp = GLOBAL_LOG_PATH / config["logger"]["data_preparation"]["path"]
open(logfile_dv, "w").close()  # erase logfile content
logger.add(
    logfile_dp,
    level=1,
    filter=lambda record: "DP" in record["extra"],
    format=f"{time_format} | {level_format} | {log_message_format}",
)