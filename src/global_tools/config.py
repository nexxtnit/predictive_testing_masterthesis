import toml
import os
from pathlib import Path

project_toml=os.environ.get('PROJECT_TO_TEST')

__config_dir = Path(__file__).parent.parent.resolve()/"config"

project_to_test = toml.load(__config_dir /project_toml)
global_config = toml.load(__config_dir / "project_config.toml")



