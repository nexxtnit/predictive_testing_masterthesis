#!/usr/bin/env python
"""
Description:
    the system tools include functions to run commands on the system
    these functions should never be used directly and should be wrapped in a class

"""

from logging import log

import subprocess
import sys
from time import sleep
from loguru import logger

import loguru
import paramiko


def cmd_run(
    command: list,
    temp_logger: loguru.logger = logger,
    execdir: str = None,
    no_output=False,
) -> list:
    """runs system command with realtime output, returns list of output"""
    output_var = []
    try:
        process = subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            cwd=execdir,
        )

        while True:
            output = process.stdout.readline()
            if output:
                output_var.append(output.decode().rstrip())
                if not no_output:
                    print(output.decode().rstrip())

            if process.poll() is not None and output == b"":
                break

        return output_var

    except subprocess.CalledProcessError as error:
        temp_logger.error("'" + " ".join(command) + "' :" + str(error))
    except OSError as error:
        temp_logger.error("'" + " ".join(command) + "' :" + str(error))
    except KeyboardInterrupt:
        sys.exit()


def ssh_cmd(
    connected_client: paramiko.SSHClient,
    command: str,
    temp_logger: loguru.logger = logger,
) -> list:
    output_var = []
    try:
        temp_logger.opt(depth=2).info(f"'{command}' :")
        stdin, stdout, stderr = connected_client.exec_command(
            command, get_pty=True
        )
        stdin.close()

        while True:
            line = stdout.readline()
            if not line:
                break
            print(line)
            output_var.append(line.rstrip())

        for line in stderr.readlines():
            temp_logger.error(f"command '{command}' returned error: ")
            print(line)

        return output_var
    except paramiko.SSHException as exception:
        temp_logger.error(str(exception))
    except KeyboardInterrupt:
        sys.exit()
