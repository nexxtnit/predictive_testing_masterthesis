#!/usr/bin/env python3
"""
machine learning training functions
"""

from pandas.core.frame import DataFrame
from src.global_tools.logger import global_logger
from pathlib import Path
from sklearn.ensemble import RandomForestClassifier
import numpy as np


def train_random_forest_classification_balanced(
    feature_df: DataFrame, labels_df: DataFrame, runs: int
) -> RandomForestClassifier:
    """balanced random forest classifier

    Args:
        feature_df (DataFrame)
        labels_df (DataFrame)
        runs (int)

    Returns:
        RandomForestClassifier
    """
    # fit(feature(x),labes(y))
    clf = RandomForestClassifier(
        n_estimators=runs, n_jobs=-1, verbose=1, class_weight="balanced"
    )
    clf.fit(np.array(feature_df), np.array(labels_df))
    return "rand_forest_class_balanced", clf


def train_random_forest_classification_default(
    feature_df: DataFrame, labels_df: DataFrame, runs: int
) -> RandomForestClassifier:
    """default random forest classifier

    Args:
        feature_df (DataFrame): 
        labels_df (DataFrame): 
        runs (int):

    Returns:
        RandomForestClassifier
    """
    # fit(feature(x),labes(y))
    clf = RandomForestClassifier(n_estimators=runs, n_jobs=-1, verbose=1)
    clf.fit(np.array(feature_df), np.array(labels_df))
    return "rand_forest_class_default", clf
