#!/usr/bin/env python3
"""
machine learning training functions
"""

from typing import Tuple
from pandas.core.frame import DataFrame
from src.global_tools.logger import global_logger
from pathlib import Path
from sklearn.ensemble import RandomForestClassifier
import numpy as np


def rf_classifier_predict(
    classifier: RandomForestClassifier,test_feature_df:DataFrame
) -> Tuple[np.array,np.array]:
    """

    Args:
        classifier (RandomForestClassifier): classificator model
        test_feature_df (DataFrame): features to use for prediction

    Returns:
        Tuple[np.array,np.array]: prediction and prediction_probability
    """
    
    predictions = classifier.predict(np.array(test_feature_df))
    y_pred_proba = classifier.predict_proba(np.array(test_feature_df))
    return predictions,y_pred_proba
