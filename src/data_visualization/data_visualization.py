#!/usr/bin/env python3
"""
Visualization of database of a repository we want to use for predictive testing

"""
from src.data_preparation.data_prep import DataPreparation
from src.global_tools.logger import global_logger

import sqlite3

from pathlib import Path
from typing import List

import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame

from bokeh.models.layouts import Column
from bokeh.models import Legend, Label
from bokeh.plotting import figure

from sklearn import metrics

from bokeh.models import Div
from bokeh.layouts import column, row, Spacer
from bokeh.models import (
    BasicTicker,
    ColorBar,
    ColumnDataSource,
    LinearColorMapper,
    NumeralTickFormatter,
)
from bokeh.transform import transform
from bokeh.palettes import inferno
from math import pi
import re

class DatabaseVisualization:
    def __init__(self, data_prep: DataPreparation):

        # init logger
        self.logger = global_logger
        self.logger = self.logger.bind(machine="host")
        self.logger = self.logger.bind(DV=True)

        ## data preparation
        self.dp = data_prep
        # load commits
        self.commits_df = self.dp.commits_df

        # load testsuite
        self.testsuite_df = self.dp.testsuite_df

        # load filediff
        self.filediff_df = self.dp.filediff_df

        # load testrun
        self.testrun_df = self.dp.testrun_df

    def plot_testresult_pcommit(
        self, repo_name, testresult_pcommit_df: pd.DataFrame,regex_tag:re
    ) -> figure:
        """create plot of passed and failed tests for all commits

        Args:
            testresult_pcommit_df (pd.DataFrame): containing CommitID,SumPassed,SumFailed

        Returns:
            figure: bokeh figure
        """

        __TOOLTIPS_TP = [
            ("Commit Nr.", " @CommitID"),
            ("Passed", "@SumPassed"),
            ("Failed", "@SumFailed"),
        ]

        # create plot figure
        plot_testres = figure(
            y_range=(
                0,
                testresult_pcommit_df.loc[
                    testresult_pcommit_df["SumFailed"] > 0
                ]["SumFailed"].min(),
            ),
            x_range=(0, len(self.dp.commitIDs)),
            sizing_mode="stretch_width",
            title=f"{repo_name}: Passed & Failed per Commit",
            tools=["pan", "hover", "wheel_zoom", "reset", "save"],
            active_drag="pan",
            toolbar_location="above",
            tooltips=__TOOLTIPS_TP,
        )
        # select plot data
        plot_testres.vbar_stack(
            stackers=["SumPassed", "SumFailed"],
            x="CommitID",
            width=1,
            source=testresult_pcommit_df[
                ["CommitID", "SumPassed", "SumFailed"]
            ].copy(),
            color=["green", "red"],
            legend_label=["PassedTests", "FailedTests"],
        )

        # style title
        plot_testres.title.text_font_size = "16pt"
        # xaxis label
        plot_testres.xaxis.axis_label = "Commit Nr. "
        # yaxis label
        plot_testres.yaxis.axis_label = "passed+failed Tests"
        # ticker values
        plot_testres.yaxis.ticker = [
            x for x in range(0, len(self.dp.testsuite_df), 50)
        ]
        plot_testres.xaxis.ticker = [
            x for x in range(0, len(self.dp.commitIDs), 100)
        ]
        plot_testres.min_border_right = 15


        CommitTagReg = re.compile(regex_tag)
        commitTags=[]
        for commitTag in self.commits_df["CommitTag"].to_list():
            if CommitTagReg.search(commitTag):
                commitTags.append(commitTag)

        commitIDs=[]
        for tag in commitTags:
            commitIDs.append(self.commits_df.loc[self.commits_df["CommitTag"]==tag]["CommitID"].values[0])

        for i in range(len(commitIDs)):
            xval=commitIDs[i]
            yval=len(self.testsuite_df.index)/2
            plot_testres.line([xval,xval], [0,yval], color="yellow", line_width=1)
            label = Label(x=xval, y=yval, y_offset=5,x_offset=3, text=commitTags[i],angle=pi/2,text_font_size="10px")
            # Implementing label in our plot
            plot_testres.add_layout(label)

        return plot_testres

    def testresult_change_pcommit(
        self, repo_name: str, testchange_pcommit_df: DataFrame
    ) -> figure:
        """create plot comparing commit size to testchange

        Args:
            repo_name  (String): name of repository
            testchange_pcommit_df (DataFrame): containing CommitID,SumPassed,SumFailed

        Returns:
            figure: bokeh figure
        """

        __TOOLTIPS_TC = [
            ("Commit Nr.", " @CommitID"),
            ("Testchange", "@SumTestChange"),
            ("Commitsize", "@CommSize"),
        ]

        plot_test_comm = figure(
            y_range=(0, max(testchange_pcommit_df["CommSize"].to_list()) + 1),
            x_range=(
                0,
                max(testchange_pcommit_df["SumTestChange"].to_list()) + 1,
            ),
            sizing_mode="stretch_width",
            title=f"{repo_name}: Commitsize vs. Testchange",
            tools=["pan", "hover", "wheel_zoom", "reset", "save"],
            active_drag="pan",
            active_scroll="wheel_zoom",
            toolbar_location="above",
            tooltips=__TOOLTIPS_TC,
        )
        plot_test_comm.circle_dot(
            y="CommSize",
            x="SumTestChange",
            size=10,
            source=testchange_pcommit_df[
                ["CommitID", "SumTestChange", "CommSize"]
            ].copy(),
            color="navy",
            alpha=0.5,
        )

        # title
        plot_test_comm.title.text_font_size = "16pt"

        # grid styling
        plot_test_comm.ygrid.visible = False
        plot_test_comm.xgrid.minor_grid_line_alpha = 0.1
        plot_test_comm.xgrid.minor_grid_line_color = "black"
        # xaxis
        plot_test_comm.xaxis.axis_label = "Test-Change"
        # yaxis
        plot_test_comm.yaxis.axis_label = "Commit-Size"
        plot_test_comm.min_border_right = 15

        return plot_test_comm

    def hist_commsize_testchange(
        self, repo_name: str, testchange_pcommit_df: pd.DataFrame
    ) -> figure:
        """create two histograms comparing commit size to testchange

        Args:
            repo_name (str): name of repository
            testchange_pcommit_df (pd.DataFrame): containing CommSize, Testchange

        Returns:
            figure: bokeh figure
        """

        max_commit_size = max(testchange_pcommit_df["CommSize"].to_list())

        __TOOLTIPS_COMSIZE = [
            ("Commit-Size", "@right"),
            ("Commit-Count", "@top"),
        ]

        histo_commsize = figure(
            sizing_mode="stretch_width",
            title=f"{repo_name}: Histogram Commitsize",
            tools=["pan", "hover", "wheel_zoom", "reset", "save"],
            active_drag="pan",
            active_scroll="wheel_zoom",
            toolbar_location="above",
            tooltips=__TOOLTIPS_COMSIZE,
        )
        hist, edges = np.histogram(
            testchange_pcommit_df["CommSize"].to_list(), bins=max_commit_size
        )
        histo_commsize.quad(
            top=hist,
            bottom=0,
            left=edges[:-1],
            right=edges[1:],
            line_color="black",
        )
        histo_commsize.title.text_font_size = "16pt"
        # yaxis label
        histo_commsize.yaxis.axis_label = "Count"
        histo_commsize.xaxis.axis_label = "Commit-Size"
        histo_commsize.xaxis.ticker = [
            x for x in range(0, max_commit_size, 10)
        ] + [max_commit_size]

        __TOOLTIPS_TESTCHANGE = [
            ("Testchange", "@right"),
            ("Commit-Count", "@top"),
        ]
        histo_testchange = figure(
            sizing_mode="stretch_width",
            title="FLIT: Histogram Testchange",
            tools=["pan", "hover", "wheel_zoom", "reset", "save"],
            active_drag="pan",
            active_scroll="wheel_zoom",
            toolbar_location="above",
            tooltips=__TOOLTIPS_TESTCHANGE,
        )
        max_testchange = max(testchange_pcommit_df["SumTestChange"].to_list())
        hist, edges = np.histogram(
            testchange_pcommit_df["SumTestChange"].to_list(),
            bins=max_testchange,
        )
        histo_testchange.quad(
            top=hist,
            bottom=0,
            left=edges[:-1],
            right=edges[1:],
            line_color="black",
        )
        histo_testchange.title.text_font_size = "16pt"
        # yaxis label
        histo_testchange.yaxis.axis_label = "Count"
        histo_testchange.xaxis.axis_label = "Amount of Testchange"
        histo_testchange.xaxis.ticker = [
            x for x in range(0, max_testchange, 10)
        ]

        return column(
            histo_commsize, histo_testchange, sizing_mode="stretch_width"
        )

    def hist_change_per_file(
        self, repo_name: str, files_with_testchange_pcommit: pd.DataFrame
    ) -> figure:
        """return histogram of filechanges per file of repository

        Args:
            repo_name (str): name of repository
            files_with_testchange_pcommit (pd.DataFrame): [description]

        Returns:
            figure: bokeh figure
        """

        # sort file names according to increasing commit size
        sorted_files = dict(
            sorted(
                files_with_testchange_pcommit.items(),
                key=lambda item: len(item[1]["CommitID"]),
            )
        )
        max_commits = max(
            [
                len(dataframe[1]["CommitID"])
                for dataframe in sorted_files.items()
            ]
        )
        __TOOLTIPS_FILE = [
            ("File", "@x"),
            ("Commit-Count", "@top"),
        ]
        commits_perfile = figure(
            y_range=[0] + [max_commits + 1],
            x_range=list(sorted_files.keys()),
            sizing_mode="stretch_width",
            title=f"{repo_name}: Commits per File",
            tools=["pan", "hover", "wheel_zoom", "reset", "save"],
            active_drag="pan",
            toolbar_location="above",
            tooltips=__TOOLTIPS_FILE,
        )
        # select plot data
        commits_perfile.vbar(
            top=[
                len(dataframe[1]["CommitID"])
                for dataframe in sorted_files.items()
            ],
            x=list(sorted_files.keys()),
            width=1,
            line_color="black",
        )
        commits_perfile.xaxis.major_label_orientation = pi / 4
        commits_perfile.yaxis.ticker = [x for x in range(0, max_commits, 50)]
        commits_perfile.min_border_right = 20
        commits_perfile.min_border_left = 100
        commits_perfile.title.text_font_size = "16pt"
        return commits_perfile

    def heatmap_testchagne_perfile(
        self, repo_name, file_with_testchange
    ) -> figure:
        data_heat = pd.DataFrame(
            file_with_testchange.stack(), columns=["TestOccur"]
        ).reset_index()

        data_heat.columns.values[0] = "Tests"
        data_heat.columns.values[1] = "Files"
        __TOOLTIPS_TC = [
            ("Test", "@Tests"),
            ("File", "@Files"),
            ("Testchange", "@TestOccur"),
        ]

        colors = list(inferno(6))
        colors.reverse()
        mapper = LinearColorMapper(
            palette=list(colors),
            low=1,
            low_color="white",
            high=data_heat["TestOccur"].max(),
        )

        heatmap1 = figure(
            sizing_mode="stretch_width",
            height=30 * len(set(data_heat["Tests"].to_list())),
            title=f"{repo_name}: Heat Files with Testchanges",
            x_range=[str(i) for i in file_with_testchange.columns.to_list()],
            y_range=[str(i) for i in file_with_testchange.index.to_list()],
            active_drag="pan",
            toolbar_location="above",
            tools=["pan", "hover", "wheel_zoom", "reset", "save"],
            x_axis_location="above",
            tooltips=__TOOLTIPS_TC,
        )
        heatmap1.rect(
            x="Files",
            y="Tests",
            width=1,
            height=1,
            source=ColumnDataSource(data_heat),
            fill_color={"field": "TestOccur", "transform": mapper},
        )

        color_bar = ColorBar(
            color_mapper=mapper,
            ticker=BasicTicker(desired_num_ticks=(len(colors))),
        )

        heatmap1.add_layout(color_bar, "right")
        heatmap1.title.text_font_size = "16pt"
        heatmap1.xaxis.major_label_orientation = pi / 4
        heatmap1.min_border_right = 100
        heatmap1.min_border_left = 50

        heatmap1.axis.axis_line_color = None
        heatmap1.axis.major_tick_line_color = None
        heatmap1.axis.major_label_text_font_size = "9pt"
        heatmap1.axis.major_label_standoff = 0
        heatmap1.xaxis.major_label_orientation = 1.0
        return heatmap1


############################
##### MACHINE LEARNING MODEL
############################


class MlVisualization:
    def __init__(self):

        # init logger
        self.logger = global_logger
        self.logger = self.logger.bind(machine="host")
        self.logger = self.logger.bind(DV=True)

    def create_roc_plot(
        self,
        fig_height: int,
        fig_width: int,
        y_pred_proba: np.ndarray,
        test_label_df: DataFrame,
    ) -> figure:

        # predict_proba=probabilities of the occurrence of each target as a tuple
        fpr, tpr, _ = metrics.roc_curve(
            np.array(test_label_df), y_pred_proba[:, 1]
        )
        auc = metrics.auc(fpr, tpr)

        __TOOLTIPS_TC = [("% false-positive", "@x"), ("% true-positive", "@y")]

        roc_plot = figure(
            height=fig_height,
            width=fig_width,
            title="Receiver Operating Characteristic (ROC)",
            x_range=[-0.01, 1],
            y_range=[0, 1.1],
            toolbar_location="right",
            tools=["pan", "hover", "reset", "save"],
            tooltips=__TOOLTIPS_TC,
        )

        auc_line = roc_plot.varea(
            fpr, tpr, [0 for i in range(len(fpr))], color="orange", alpha=0.5
        )
        worst_roc = roc_plot.line(
            [0.0, 1.0], [0.0, 1.0], line_color="red", line_width=1.5
        )
        best_roc = roc_plot.line(
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 1.0],
            line_color="green",
            line_width=1.5,
            alpha=0.8,
        )
        roc_line = roc_plot.line(fpr, tpr, line_width=3.5, alpha=0.8)

        legend = Legend(
            items=[
                ("worst_roc", [worst_roc]),
                ("best_roc", [best_roc]),
                ("roc_user", [roc_line]),
                (f"auc={round(auc,3)}", [auc_line]),
            ],
            location=(600, 0),
            orientation="vertical",
        )

        roc_plot.add_layout(
            legend,
        )
        roc_plot.title.text_font_size = "12pt"
        roc_plot.xaxis.axis_label = "% False Positive"
        roc_plot.yaxis.axis_label = "% True Positive"
        return roc_plot

    def calc_confusion_matrix(
        self, test_label_df, predictions, labels: List[str]
    ) -> DataFrame:
        matrix = metrics.confusion_matrix(test_label_df, predictions)
        matrix = matrix.astype("float") / matrix.sum(axis=1)[:, np.newaxis]
        return pd.DataFrame(matrix, columns=labels, index=labels) * 100

    def calc_conf_matrix_absnumbers(self, test_label_df, predictions, labels: List[str])->DataFrame:
        matrix = metrics.confusion_matrix(test_label_df, predictions)
        matrix = matrix.astype("int")
        return pd.DataFrame(matrix, columns=labels, index=labels)

    def create_confusionmatr_heatmap(
        self, fig_height: int, fig_width: int, matrix_df: DataFrame
    ):
        from bokeh.models import (
            BasicTicker,
            ColorBar,
            ColumnDataSource,
            LabelSet,
            LinearColorMapper,
        )
        from bokeh.palettes import YlGn9

        # Prepare data.frame in the right format
        df = matrix_df.stack().rename("Accuracy").reset_index()
        df.columns.values[0] = "Labels"
        df.columns.values[1] = "Predictions"
        df["textacc"] = df["Accuracy"].astype(str)
        source = ColumnDataSource(df)

        __TOOLTIPS_TC = [("accuracy", "@Accuracy %")]
        colors = list(YlGn9)
        colors.reverse()
        mapper = LinearColorMapper(
            palette=colors,
            low=0,
            high=df["Accuracy"].max(),
        )

        conf_matrix = figure(
            height=fig_height,
            width=fig_width,
            title="Confusion Matrix",
            x_range=[str(i) for i in matrix_df.columns.to_list()],
            y_range=[str(i) for i in reversed(matrix_df.index.to_list())],
            toolbar_location="above",
            tools=["hover", "reset", "save"],
            x_axis_location="above",
            tooltips=__TOOLTIPS_TC,
        )

        conf_matrix.rect(
            x="Predictions",
            y="Labels",
            width=1,
            height=1,
            source=source,
            fill_color={"field": "Accuracy", "transform": mapper},
        )

        color_bar = ColorBar(
            color_mapper=mapper,
            ticker=BasicTicker(desired_num_ticks=(len(colors))),
        )

        labels = LabelSet(
            x="Predictions",
            y="Labels",
            text="textacc",
            level="overlay",
            x_offset=-15,
            y_offset=-5,
            source=source,
            text_font_size="10pt",
            text_color="black",
            render_mode="canvas",
        )

        conf_matrix.add_layout(labels)
        conf_matrix.add_layout(color_bar, "right")
        conf_matrix.title.text_font_size = "12pt"
        conf_matrix.min_border_right = 100
        conf_matrix.min_border_left = 50
        conf_matrix.xaxis.axis_label = "Predictions"
        conf_matrix.yaxis.axis_label = "Labels"
        conf_matrix.axis.axis_line_color = None
        conf_matrix.axis.major_tick_line_color = None
        conf_matrix.axis.major_label_text_font_size = "9pt"
        conf_matrix.axis.major_label_standoff = 0

        return conf_matrix

    def rand_forest_analysis(
        self,
        predictions: np.ndarray,
        y_pred_proba: np.ndarray,
        test_label_df: DataFrame,
        title: str,
    ) -> Column:
        """
        create
        """

        classification_report = metrics.classification_report(
            np.array(test_label_df), predictions
        )

        ### classification report
        class_rep = classification_report.replace("\n", "<br>")

        ### roc plot
        roc_fig = self.create_roc_plot(400, 830, y_pred_proba, test_label_df)

        ### confusion matrix
        conf_matrix_df = self.calc_confusion_matrix(
            test_label_df, predictions, ["Failed", "Passed"]
        )
        conf_matrix = self.create_confusionmatr_heatmap(
            250, 350, conf_matrix_df
        )

        title_string = Div(text=f"<h1 style='text-align: center'>{title}</h1>")
        classification_report_div = Div(
            text=f"<b><pre>{class_rep}</pre><b>",
            width=420,
            background=None,
            margin=(40, 0, 0, 20),
        )
        rand_forest_analysis = column(
            title_string, roc_fig, row(conf_matrix, classification_report_div)
        )
        return rand_forest_analysis